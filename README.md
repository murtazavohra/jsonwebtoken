# JSONWebToken

[![CI Status](https://img.shields.io/travis/Murtaza/JSONWebToken.svg?style=flat)](https://travis-ci.org/Murtaza/JSONWebToken)
[![Version](https://img.shields.io/cocoapods/v/JSONWebToken.svg?style=flat)](https://cocoapods.org/pods/JSONWebToken)
[![License](https://img.shields.io/cocoapods/l/JSONWebToken.svg?style=flat)](https://cocoapods.org/pods/JSONWebToken)
[![Platform](https://img.shields.io/cocoapods/p/JSONWebToken.svg?style=flat)](https://cocoapods.org/pods/JSONWebToken)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JSONWebToken is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JSONWebToken'
```

## Author

Murtaza, ma.murtaza@icloud.com

## License

JSONWebToken is available under the MIT license. See the LICENSE file for more info.
